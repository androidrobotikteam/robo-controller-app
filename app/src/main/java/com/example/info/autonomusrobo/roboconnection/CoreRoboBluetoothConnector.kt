package com.example.info.autonomusrobo.roboconnection

import android.app.AlertDialog
import android.bluetooth.BluetoothAdapter
import android.content.Context
import android.content.DialogInterface
import android.R.attr.action
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothDevice.*
import android.content.Intent
import android.content.BroadcastReceiver
import android.content.IntentFilter


class CoreRoboBluetoothConnector {
    private var bluetoothAdapter: BluetoothAdapter;
    private var context: Context;
    private var devices: MutableList<BluetoothDevice> = mutableListOf<BluetoothDevice>();

    constructor(context: Context) {
        this.context = context;
        this.bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    }

    fun scanForBluetoothDevices(devicesCallback: (d: List<BluetoothDevice>) -> Unit) {
        if (this.bluetoothAdapter == null) {
            AlertDialog.Builder(this.context)
                    .setTitle("Not compatible")
                    .setMessage("Your phone does not support Bluetooth")
                    .setPositiveButton("Exit", DialogInterface.OnClickListener { dialog, which -> System.exit(0) })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
            devicesCallback(emptyList());
        } else {
            // If devices are connected, show them in the list
            if (this.bluetoothAdapter.bondedDevices.isNotEmpty()) {
                this.devices.addAll(this.bluetoothAdapter.bondedDevices);
                devicesCallback(devices);
            }
            if (this.bluetoothAdapter.isDiscovering) {
                this.bluetoothAdapter.cancelDiscovery();
            }
            this.bluetoothAdapter.startDiscovery();
            val ifilter = IntentFilter(BluetoothDevice.ACTION_FOUND);
            val bluetoothReciever = object : BroadcastReceiver() {
                override fun onReceive(context: Context, intent: Intent) {
                    val action = intent.action;
                    if (BluetoothDevice.ACTION_FOUND == action) {
                        val device = intent.getParcelableExtra<BluetoothDevice>(BluetoothDevice.EXTRA_DEVICE)
                        if (device.bondState != BluetoothDevice.BOND_BONDED) {
                            devices.add(device);
                        }
                    } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED == action) {
                        devicesCallback(devices);
                    }
                }
            }
            this.context.registerReceiver(bluetoothReciever, ifilter);
        }
    }

    fun stopScanning() {
        if (this.bluetoothAdapter != null && this.bluetoothAdapter.isDiscovering) {
            this.bluetoothAdapter.cancelDiscovery();
        }
    }
}