package com.example.info.autonomusrobo

import android.bluetooth.BluetoothDevice
import android.content.res.Resources
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.info.autonomusrobo.roboconnection.CoreRoboBluetoothConnector

class WelcomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)
        val bl: CoreRoboBluetoothConnector = CoreRoboBluetoothConnector(this);
        bl.scanForBluetoothDevices { d: List<BluetoothDevice> ->
            {
                Log.i("devices",d.joinToString { "," });
            }
        };
    }

    internal fun onButtonClicked() {

    }
}
